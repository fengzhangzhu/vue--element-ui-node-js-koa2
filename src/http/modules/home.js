import $http from '../http'

//获取home页面信息
const bannerList = (data) => {
    return $http({
        url:'/api/home/bannerInfo',
        method:'get',
        data
    })
}

const EditBannerList = (data) => {
    return $http({
        url:'/api/home/addHomeInfo',
        method:'post',   
        data
    })
}

export default {
    bannerList,
    EditBannerList
}