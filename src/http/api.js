import login from './modules/login'
import menu from './modules/menu'
import userList from './modules/user'
import role from './modules/role'
import home from './modules/home'

export default{
    ...login,
    ...menu,
    ...userList,
    ...role,
    ...home
}