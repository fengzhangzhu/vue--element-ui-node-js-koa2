import FormBox from './FormBox.vue';
import OutBox from './Outbox.vue';
import FormInput from './FormInput.vue';

export {
    FormBox,
    OutBox,
    FormInput
}