module.exports = {
    devServer: {
        port: process.env.VUE_APP_PORT,     // 端口号
    },
    configureWebpack: {
        devtool: "source-map",
        resolve: {
            extensions: ['.js', '.vue', '.json']
        }
    }

};
